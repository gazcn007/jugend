from image import *
image=file2image('ivy.png')
image = [[x for x in row] for row in image]
gray = [[int(0.7152*p[0] + 0.2126*p[1] + 0.0722*p[2]) for p in row]
                                                          for row in image]
# I used a grayscale conversion similar to HDTV grayscale conversion as per https://en.wikipedia.org/wiki/Grayscale
# The only difference is the switch between red and green because of my love, Ivy, the girl of reddish. 
image2display(gray)
for x in range(len(gray)):
    for y in range(len(gray[0])):
        if gray[x][y] < 150: 
            gray[x][y]=0
        else:
            gray[x][y]=225
BI=gray
image2display(BI)
