var fs=require('fs');

module.exports.convert2String = convert2String;
module.exports.convert2png=convert2png;

//address is where the picture is saved
function convert2StringCB (address,callback)
{
  var imageBuf=fs.readFileSync(address);
  var mybuffer=new Buffer(imageBuf,'base64');
  callback(mybuffer);
}

function convert2String (address)
{
  var imageBuf=fs.readFileSync(address);
  var mybuffer=new Buffer(imageBuf,'base64');
  return mybuffer;
}

//address is where to put the png file
//string is the base64 based png string
//name is the name of the new png you want it to be
function convert2png (address,string,name)
{
  fs.writeFile(address+name,string);
}

//example call to convert McGill logo to current directory
//--> convert2StringCB('../School_Logo/universityLogos/McGill.png',function(string){convert2png("./",string,"McGill12.png");});
//-->convert2png('./',convert2String('../School_Logo/universityLogos/McGill.png'),'rock');
