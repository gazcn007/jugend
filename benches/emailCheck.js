
var fs = require('fs'),filename = "EmailCheck.txt";
var redis=require('./redis.js');
var png=require('./64BasedPngString.js');
module.exports.checkEmail = getEmail;
//module.exports.initEmailInfo = initEmailInfo;

//get check if email works
function getEmail(string,callback){
	var check=false;
	var strArray = string.split("@");
  fs.readFile(filename, 'utf8', function(err, data1){
	    if (err)
	    {
	      throw err;
	    }
	    else
	    {
	      i = data1.search(strArray[1]);
	      if(i == -1){
	      	check= false;
	        }
	      else
	      {check= true;}
	    }
  	callback(check);
	});
}

//initialize Redis with email-school Emailinfo.json file
function initEmailInfo(){
	fs.readFile("./EmailInfo.json",function(err,data){
		if(err)
			throw err;
		var jsonObj=JSON.parse(data);
		var n=0;
		for (var i in jsonObj)
		{	redis.initializeEmail2School(i,jsonObj[i].school);
			if (jsonObj[i].logoPNG!==''){
			string=png.convert2String(jsonObj[i].logoPNG);
			redis.initializeSchool2Png(i,string);
			}
		}

	});
}


//examples:
//getEmail("Troy@mcgilsl.ca",function(check){console.log(check);});
//initEmailInfo("mcgill.ca",function(check){console.log(check);});
initEmailInfo();
