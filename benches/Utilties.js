
var redis=require('./redis.js');
var png=require('./64BasedPngString.js');
var fs = require('fs');

//get check if email works
exports.checkEmail =function getEmail(string,callback){
	var filename = "./Data/EmailCheck.txt";
	var check=false;
	var strArray = string.split("@");
  fs.readFile(filename, 'utf8', function(err, data1){
	    if (err)
	    {
	      throw err;
	    }
	    else
	    {
	      i = data1.search(strArray[1]);
	      if(i == -1){
	      	check= false;
	        }
	      else
	      {check= true;}
	    }
  	callback(check);
	});
};

//initialize Redis with email-school Emailinfo.json file
exports.initEmailInfo=function initEmailInfo(callback){
	fs.readFile("./Data/EmailInfo.json",function(err,data){
		if(err)
			throw err;
		var jsonObj=JSON.parse(data);
		for (var i in jsonObj)
		{	redis.initializeEmail2School(i,jsonObj[i].school);
			if (jsonObj[i].logoPNG!==''){
			string=png.convert2String(jsonObj[i].logoPNG);
			redis.initializeSchool2Png(i,string);
			}
		}
	});
	callback("succes");
};

exports.initFakeUser=function initFakeUser(callback){
	fs.readFile("./Data/FakeUser.json",function(err,data){
		if(err)
			throw err;
		var jsonObj=JSON.parse(data);
		for (var i in jsonObj)
		{	redis.SET_USER(i,jsonObj[i].Name,jsonObj[i].School,jsonObj[i].Email,jsonObj[i].GPA);
			}
	});
	callback("succes");
};

//Algorithms in JavaScripts
//We can also implement algorithms in LUA on redis
module.exports.getMean=getMean();
function getMean (array){
  var n=0, sum=0;
  for (var i in array)
  {
    sum=sum+array[i];
    n=n+1;
  }
  return sum/n;
}

module.exports.getVariance=getVariance();
function getVariance(array,mean){
	var counter=0;
	var sum=0;
	for (var i in array)
	{
	sum=sum+Math.pow((array[i]-mean),2);
	counter=counter+1;
	}
	return sum/counter;
}

module.exports.normalPDF=normalPDF();
function normalPDF(mean,variance,x){
	return (1.0/ (variance * Math.pow((2.0*3.1415926),0.5))) * Math.exp((-0.5) * Math.pow((( x - mean)/variance),2.0));
}

module.exports.normalCDF=normalCDF();
function normalCDF(mean,variance,x){
	//set a starting point, considering gpa is 0.0-4.0 a left spread of -99.9 from the mean will work
	var counter = mean-99.9;
	var sum=0;
	while(counter<=x)
	{
		sum=sum+(normalPDF(mean,variance,counter)*0.001);
		counter=counter+0.001;
	}
	return sum;
}

module.exports.csvParser=csvParser();
function csvParser(filename){
  fs.readFile(filename, 'utf8', function(err, data){
	    if (err)
	    {
	      throw err;
	    }
	    else
	    {
	      console.log("1");
	    }
	});
}

csvParser('./Data/uwaterloo_courses.csv');
//example for normal distribution
//->1. var mean=getMean ([1.0,2.0,3.0,4.0]);
//->2. var variance=getVariance ([1.0,2.0,3.0,4.0],mean);
//->3. console.log(normalCDF(mean,variance,3.0));
