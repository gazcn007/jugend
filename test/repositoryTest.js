var assert = require('assert'),
    client = require('redis').createClient('test'),
    repo = require('../benches/addJob');

describe('Repository Test', function(){

  beforeEach(function(){
    client.flushdb();
  });

  afterEach(function(){
    client.flushdb();
  });

  

    //setup the data
    client.set('default:users:josh', 'josh');
    client.set('default:users:josh:vote', JSON.stringify({test: 'test'}));

  it('should remove the user with removeUser', function(done){
    client.sadd('default:users', 'default:users:josh');

    var rem = repo.removeUser('josh', 'default', client);
    rem.done(function(){
      client.smembers('default:users', function(err, users){
        assert.equal(users.length, 0);
        done();
      })
    });
  });

  it('setUser should set username', function(done){
    var user = repo.setUser('josh', 'default', 7200, client);
    user.done(function(){
      client.get('default:users:josh', function(e, d){
        assert.equal(d, 'josh');
        done();
      });
    });
  });

  it('should set a string in the set', function(done){
    var user = repo.setUser('josh', 'default', 7200, client);
    user.done(function(){
      client.smembers('default:users', function(e, d){
        assert.equal(d.length, 1);
        done();
      });
    });
  });

  it('should set ttl on the key', function(done){
    var user = repo.setUser('josh', 'default', 7200, client);
    user.done(function(){
      client.ttl('default:users:josh', function(e, d){
        //make sure a ttl is set
        assert.equal(d <= 7200, true);
        assert.equal(d > 1, true);
        done();
      });
    });
  });

  it('should expire the user key', function(done){
    var user = repo.setUser('josh', 'default', 0, client);
    user.done(function(){
      client.get('default:users:josh', function(e, d){
        assert.equal(d, null);
        client.scard('default:users', function(e, d){
          assert.equal(d, '0');
          done();
        });
      });
    });
  });


});
